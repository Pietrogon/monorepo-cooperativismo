package com.cwi.cooperativismo.configuration;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build()
        .useDefaultResponseMessages(false)
        .globalResponseMessage(RequestMethod.GET, responseMessageForGET())
        .apiInfo(apiInfo());
  }

  private List<ResponseMessage> responseMessageForGET() {
    return new ArrayList<ResponseMessage>() {{
      add(new ResponseMessageBuilder()
          .code(500)
          .message("500 message")
          .responseModel(new ModelRef("Error"))
          .build());
      add(new ResponseMessageBuilder()
          .code(403)
          .message("Forbidden!")
          .build());
    }};
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Cooperativismo")
        .description("No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias")
        .version("1.0.0")
        .license("MIT License")
        .licenseUrl("https://gitlab.com/Pietrogon/monorepo-cooperativismo/-/blob/main/LICENSE")
            .contact(new Contact("Pietro Gonçalves", "https://www.linkedin.com/in/pietrogon/","pietrogon@gmail.com.br"))
            .build();
  }
}